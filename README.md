# simon-lab-fir

Finite Impulse Response (FIR) filter implemented on a Red Pitaya FPGA based on a project from the Simon Lab.

## Installation
1. Get a Red Pitaya and perpare and SD card with the official image according to 
https://redpitaya.readthedocs.io/en/latest/quickStart/SDcard/SDcard.html.

2. Turn on the Red Pitaya and hook it up to your local network. Find the ip address
or hostname of the device. The hostname will be based on the MAC address, e.g. if the 
MAC address is 00:26:32:F0:03:34 then the hostname is "rp-f00334". 

3. Open up a web browser and have a look at the website of the Red Pitaya which is
served for example under http://rp-f00334.lab.

4. SSH into your device with ```ssh root@rp-f00334.lab```, the password is ```root```.

5. Change the default root password to Ca+=... with ```passwd```.

6. 

